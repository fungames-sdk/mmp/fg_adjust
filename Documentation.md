# Adjust

## Integration Steps

1) **"Install"** or **"Upload"** FG Adjust plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Follow the instructions in the **"Install External Plugin"** section to import Adjust SDK.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) To finish your integration, follow the **Account and Settings** section.

## Install External Plugin

After importing the FG Adjust module from the Integration Manager window, you will find the last compatible version of Adjust SDK in the _Assets > FunGames_Externals > MMP folder. Double click on the .unitypackage file to install it.

If you wish to install a newest version of their SDK, you can also download it from <a href="https://github.com/adjust/unity_sdk/releases" target="_blank">here</a> (please take the last version of the SDK and download Adjust_vX.X.X.unitypackage file, not the Imei and Oaid one !).

**Note that the versions of included external SDKs are the latest tested and approved by our development team. We recommend using these versions to avoid any type of discrepencies. If, for any reason, you already have a different version of the plugin installed in your project that you wish to keep, please advise our dev team.**

## Account and Settings

There is nothing to set up beforehand. In order to start Adjust integration, your TapNation manager will have to create your application on their dashboard. In order to help them do that, please provide the following information:

- For Android The application’s Bundle Id
- For iOS The application’s Bundle Id and Apple Store ID
- The email addresses you wish to receive the integration documents on

Once your application is created on TapNation’s side, you will receive an email from Adjust with the token of your application, or you can ask it directly to your UA Manager.

Fill up FG Adjust Settings (_Assets > Resources > FunGames > FGAdjustSettings_) with the token of your app, and **make sure that “Start SDK Manually” is enabled** in the Adjust prefab. After that, all the settings for adjust will be handled in the FG Adjust Settings.

![](_source/adjust_startSdkManually.png)

Once Adjust has been initiated in your application, you will need to test that everything works fine. To do that, play your app on your phone and send your IDFA/GAID to your manager so that he can check if Adjust is properly receiving data from your device.

By default, Adjust environment is now set to Production so there is nothing else to setup before releasing. If you want to switch to Sandbox for a specific device, you can create a Remote Config, with the corresponding device ID, setting 'AdjustSandbox' variable to 1.